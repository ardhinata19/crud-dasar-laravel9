<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\ProdiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('/students' , MahasiswaController::class);
//Route::resource('/prodi' , ProdiController::class);

//Mahasiswa
Route::prefix('students')->group(function () {
    Route::get('/index', [MahasiswaController::class,'index']);
    Route::get('/create', [MahasiswaController::class,'create']);
    Route::post('/store', [MahasiswaController::class,'store']);
    Route::get('{id}/show', [MahasiswaController::class,'show']);
    Route::get('{id}/edit', [MahasiswaController::class,'edit']);
    Route::put('{id}/update', [MahasiswaController::class,'update']);
    Route::delete('{id}/delete', [MahasiswaController::class,'destroy']);
});

//Prodi
Route::prefix('prodi')->group(function () {
    Route::get('/index', [ProdiController::class,'index']);
    Route::get('/create', [ProdiController::class,'create']);
    Route::post('/store', [ProdiController::class,'store']);
//    Route::get('{id}/show', [ProdiController::class,'show']);
    Route::get('{id}/edit', [ProdiController::class,'edit']);
    Route::put('{id}/update', [ProdiController::class,'update']);
    Route::delete('{id}/delete', [ProdiController::class,'destroy']);
});
