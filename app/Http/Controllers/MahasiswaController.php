<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\Prodi;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Mahasiswa::select("mahasiswa.*", 'prodi.nama_prodi as prodi')
            ->join('prodi', 'prodi.id', '=', 'mahasiswa.prodi_kode')->orderBy('mahasiswa.id')->get();
        $prodi = Prodi::latest()->get();
//        dd($students);
        return view('students.index', compact('students', 'prodi'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prodi = Prodi::all();
        return view('students.create')->with('prodi', $prodi);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
//        dd($input);
        Mahasiswa::create($input);
        return redirect('students/index')->with('flash_message', 'Mahasiswa Addedd!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $students = Mahasiswa::findOrFail($id);
        return view('students.show',compact('students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Mahasiswa::select("mahasiswa.*", 'prodi.nama_prodi as prodi')
            ->where('mahasiswa.id','=',$id)
            ->join('prodi', 'prodi.id', '=', 'mahasiswa.prodi_kode')
            ->orderBy('mahasiswa.id')->get();
        $prodi = Prodi::latest()->get();
        return view('students/edit', compact('students','prodi'));
//        return view('students/edit',)->with('students',implode([$students]));

//        dd(implode([$students]));
//        $students = Mahasiswa::all()->where('id','=',$id);
//
//        $students = Mahasiswa::all()
//            ->where('id','=',$id);
//           dd($students);
//        return view('students.edit', compact('students'));

//        $prodi = Prodi::select("prodi.*")->join()->where('prodi', 'prodi.id', '=', 'mahasiswa.prodi_kode')
//            $students = Mahasiswa::find($id);
//        return view('students.edit', compact('students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Mahasiswa::findOrFail($id);
        $input = $request->all();
//        $student->nim = $request->nim;
//        $student->prodi_kode = $request->prodi_kode;
//        $student->nama = $request->nama;
//        $student->tgl_lahir = $request->tgl_lahir;
//        $student->jk = $request->jk;
//        $student->agama = $request->agama;
//        $student->alamat = $request->alamat;
//        dd($student);
        $student->update($input);
        return redirect('students/index')->with('flash_message', 'Mahasiswa Updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mahasiswa::destroy($id);
        return redirect('students/index');
    }
}
