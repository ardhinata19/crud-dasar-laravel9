<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{

    protected $table = 'mahasiswa';
    protected $primaryKey = 'id';
    protected $fillable = ['nim','prodi_kode', 'nama', 'tgl_lahir', 'jk','agama', 'alamat'];
}
