<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{

    protected $table = 'prodi';
    protected $primaryKey = 'id';
    protected $fillable = ['kode_prodi', 'nama_prodi'];
}
