@extends('students.layout')
@section('content')
    @if(isset($students))
        @foreach($students as $row)
    <div class="card" style="margin: 20px">
        <div class="card-header">Edit Student</div>
        <div class="card-body">
            <form action="{{url('students/' . $row->id . '/update')}}" method="post">
                {!! csrf_field() !!}
                @method("PUT")
                <label>NIM</label>
                <input type="number" name="nim" id="nim" value="{{$row->nim}}" class="form-control" required><br>
                <div class="form-group">
                    <label for="category" class="form-label">Prodi</label>
                    <select name="prodi_kode" id="prodi_kode" class="form-control">
                        <option disabled selected value="{{ $row->prodi_kode }}">{{ $row->prodi }}</option>
                        @foreach($prodi as $item)
                            <option value="{{ $item }}">{{ $item->nama_prodi }}</option>
                        @endforeach
                    </select>
                </div><br>
                <label>Nama</label>
                <input type="text" name="nama" id="nama" value="{{$row->nama}}" class="form-control" required><br>
                <label>Tanggal Lahir</label>
                <input type="date" name="tgl_lahir" id="tgl_lahir" value="{{$row->tgl_lahir}}" class="form-control"><br>
                <div class="form-group">
                    <label for="form_jk">Jenis Kelamin</label>
                    <select name="jk" class="form-control">
                        <option disabled selected>{{$row->jk}}</option>
                        <option value="Laki-laki">Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
                <div class="form-group"><br>
                    <label for="form_agama">Agama</label>
                    <select name="agama" class="form-control" required>
                        <option disabled selected>{{$row->agama}}</option>
                        <option value="Katolik">Katolik</option>
                        <option value="Protestan">Protestan</option>
                        <option value="Islam">Islam</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Buddha">Buddha</option>
                        <option value="Khonghucu">Khonghucu</option>
                    </select>
                </div><br>
                @endforeach
                @endif
                <label>Alamat</label>
                <input type="text" name="alamat" id="alamat" value="{{$row->alamat}}" class="form-control" required><br>
                <input type="submit" value="save" class="btn btn-success"><br>
            </form>
        </div>
    </div>
    @stop
