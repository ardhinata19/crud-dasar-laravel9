@extends('students.layout')
@section('content')
    <div class="container">
        <div class="row" style="margin: 20px">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h2>Students CRUD</h2>
                    </div>
                    <div class="card-body">
                        <a href="{{url('/students/create')}}" class="btn btn-sm btn-success" title="Add New Student">Add New</a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>NIM</th>
                                    <th>Prodi</th>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->nim}}</td>
                                        <td>{{$item->prodi}}</td>
                                        <td>{{$item->nama}}</td>
                                        <td>
                                            <a href="{{url('/students/' . $item->id . '/show')}}" title="View Student"><button class="btn btn-warning btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{url('/students/' . $item->id . '/edit')}}" title="Update Student"><button class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update</button></a>
                                           <form method="POST" action="{{url('/students/' . $item->id . '/delete')}}" accept-charset="UTF-8" style="display:inline">
                                               {{method_field('DELETE')}}
                                               {{csrf_field()}}
                                               <button type="submit" class="btn btn-danger btn-sm" title="Delete Student" onclick="return"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                           </form>
                                        </td>
                                    </tr>
                               @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

