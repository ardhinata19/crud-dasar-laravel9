@extends('students.layout')
@section('content')
    <div class="card" style="margin: 20px">
        <div class="card-header">Student Page</div>
        <div class="card-body">
            <p class="card-title">Nama : {{$students->nama}}</p>
            <p class="card-text">NIM : {{$students->nim}}</p>
            <p class="card-text">Jenis Kelamin : {{$students->jk}}</p>
            <p class="card-text">Agama : {{$students->agama}}</p>
            <p class="card-text">Tanggal lahir : {{$students->tgl_lahir}}</p>
            <p class="card-text">Alamat : {{$students->alamat}}</p>
        </div>
    </div>
@stop
