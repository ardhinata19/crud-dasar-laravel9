@extends('students.layout')
@section('content')
    <div class="card" style="margin: 20px">
        <div class="card-header">Create New Student</div>
        <div class="card-body">
            <form action="{{url('/students/store')}}" method="post">
                {!! csrf_field() !!}
                <label>NIM</label>
                <input type="number" name="nim" id="nim" class="form-control"><br>
                <div class="form-group">
                    <label for="category" class="form-label">Prodi</label>
                    <select name="prodi_kode" id="prodi_kode" class="form-control">
                        <option disabled selected>--Pilih--</option>
                        @foreach($prodi as $row)
                            <option value="{{ $row->id }}">{{ $row->nama_prodi }}</option>
                        @endforeach
                    </select>
                </div><br>
                <label>Nama</label>
                <input type="text" name="nama" id="nama" class="form-control"><br>
                <label>Tanggal Lahir</label>
                <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control"><br>
                <div class="form-group">
                    <label for="form_jk">Jenis Kelamin</label>
                    <select name="jk" class="form-control">
                        <option disabled selected>--Pilih--</option>
                        <option value="Laki-laki">Laki-Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
                <div class="form-group"><br>
                    <label for="form_agama">Agama</label>
                    <select name="agama" class="form-control">
                        <option disabled selected>--Pilih--</option>
                        <option value="Katolik">Katolik</option>
                        <option value="Protestan">Protestan</option>
                        <option value="Islam">Islam</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Buddha">Buddha</option>
                        <option value="Khonghucu">Khonghucu</option>
                    </select>
                </div><br>
                <label>Alamat</label>
                <input type="text" name="alamat" id="alamat" class="form-control"><br>
                <input type="submit" value="save" class="btn btn-success"><br>
            </form>
        </div>
    </div>
    @stop
