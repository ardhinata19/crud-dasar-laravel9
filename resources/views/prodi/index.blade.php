@extends('students.layout')
@section('content')
    <div class="container">
        <div class="row" style="margin: 20px">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h2>PRODI CRUD</h2>
                    </div>
                    <div class="card-body">
                        <a href="{{url('/prodi/create')}}" class="btn btn-sm btn-success" title="Add New Student">Add New</a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kode Prodi</th>
                                    <th>nama Prodi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($prodi as $row)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$row->kode_prodi}}</td>
                                        <td>{{$row->nama_prodi}}</td>
                                        <td>
                                            <a href="{{url('/prodi/' . $row->id . '/edit')}}"><button class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update</button></a>
                                           <form method="POST" action="{{url('/prodi/' . $row->id . '/delete')}}" accept-charset="UTF-8" style="display:inline">
                                               {{method_field('DELETE')}}
                                               {{csrf_field()}}
                                               <button type="submit" class="btn btn-danger btn-sm"  onclick="return"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                           </form>
                                        </td>
                                    </tr>
                               @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

