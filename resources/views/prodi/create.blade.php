@extends('students.layout')
@section('content')
    <div class="card" style="margin: 20px">
        <div class="card-header">Prodi Baru</div>
        <div class="card-body">
            <form action="{{url('/prodi/store')}}" method="post">
                {!! csrf_field() !!}
                <label>Kode Prodi</label>
                <input type="text" name="kode_prodi" id="kode_prodi" class="form-control"><br>
                <label>Nama Prodi</label>
                <input type="text" name="nama_prodi" id="nama_prodi" class="form-control"><br>
                <input type="submit" value="save" class="btn btn-success"><br>
            </form>
        </div>
    </div>
    @stop
